function ApplicationUpdateTool(param) {
	var appid = param["appid"];
	var vcode = param["vcode"];
	var apihost = param["apihost"];
	var sysinfo = param["sysinfo"];
	// var param = {
	// 	"apihost": ____apihost,
	// 	"pvcode": 0,
	// 	"vcode": plus.runtime.versionCode,
	// 	"sysinfo": JSON.stringify(____clientStaticData),
	// 	"appid": VisualConst.getCustomDevice()["appid"]
	// };
	// var appStoreURL_cn = "https://itunes.apple.com/cn/lookup?id=" + appStoreId;
	// var appStoreURL = "https://itunes.apple.com/lookup?id=" + appStoreId;

	var appStoreURL_cn = "https://itunes.apple.com/cn/lookup";
	var appStoreURL = "https://itunes.apple.com/lookup";

	var apkURL = function(version) {
		return apihost + "/client/checkUpdate";
	};

	var wgtURL = function(type, version) {
		return apihost + "/client/checkUpdate";
	};

	function ajaxData(url, success, error) {
		mui.ajax(url, {
			data: param,
			dataType: 'json',
			type: 'post',
			timeout: 20000,
			processData:true,
			success: function(data) {
				success(data);
			},
			error: function(xhr, type, errorThrown) {
				success(error);
			}
		});
	}

	/**
	 * 检查安装包
	 * CallBackWgtUpdate - 回调检查wgt更新，如果有安装包更新，就不会回调
	 */
	function examinationInstallationPackage(CallBackWgtUpdate) {
		function toNum(a) {
			var a = a.toString();
			var c = a.split('.');
			var num_place = ["", "0", "00", "000", "0000"],
				r = num_place.reverse();
			for (var i = 0; i < c.length; i++) {
				var len = c[i].length;
				c[i] = r[len] + c[i];
			}
			var res = c.join('');
			return res;
		}

		function cpr_version(newV, oldV) {
			var _a = toNum(newV),
				_b = toNum(oldV);
			if (_a > _b) {
				return true;
			} else {
				return false;
			}
		}
		if (plus.os.name == 'Android') {
			mui.ajax(apkURL(), {
				data: param,
				dataType: 'json',
				type: 'post',
				timeout: 20000,
				processData:true,
				success: function(data) {
					console.log(JSON.stringify(data));
					if (data && data["code"] ==0 && data["data"] && null !=data["data"]["update_url"] && data["data"]["update_url"].indexOf(".apk") >= 0) {
						mui.confirm(data["data"]["update_content"], '发现新版本', ['立即更新', '下次再说'], function(e) {
							if (e.index == 0) {
								//data = data.replace(new RegExp("\"", 'g'), "");
								downWgtOrApk(data["data"]["update_url"], false);
							} else {
								return;
							}
						})
					} else {
						CallBackWgtUpdate();
					}
				},
				error: function(xhr, type, errorThrown) {
					//console.log("error")
				}
			});
		} else if (plus.os.name == 'iOS') {
			var data_cn;
			ajaxData(appStoreURL_cn, function(data) {
				data_cn = data;
				ajaxData(appStoreURL, function(data) {

					var currentVersionReleaseDate_cn = "";
					var currentVersionReleaseDate = "";

					if (data_cn != null) {
						currentVersionReleaseDate_cn = data_cn.results[0].currentVersionReleaseDate;
						if (currentVersionReleaseDate_cn.indexOf('T') >= 0) {
							currentVersionReleaseDate_cn = currentVersionReleaseDate_cn.split('T')[0];
						}
					}
					if (data != null) {
						currentVersionReleaseDate = data.results[0].currentVersionReleaseDate;
						if (currentVersionReleaseDate.indexOf('T') >= 0) {
							currentVersionReleaseDate = currentVersionReleaseDate.split('T')[0];
						}
					}
					var date_cn = new Date(currentVersionReleaseDate_cn);
					var date = new Date(currentVersionReleaseDate_cn);
					var newData;
					if (data_cn > date) {
						newData = data_cn;
					} else {
						newData = data;
					}

					if (newData != null) {
						var newV = 0;
						try {
							newV = newData.results[0].version;
						} catch (e) {}

						if (cpr_version(newV, oldV)) {
							mui.confirm('发现ios新版本', '检查更新', ['立即更新', '下次再说'], function(e) {
								if (e.index == 0) {
									try {
										var trackViewUrl = newData.results[0].trackViewUrl;
										trackViewUrl = trackViewUrl.replace("https", "itms-apps");
										plus.runtime.openURL(trackViewUrl);
									} catch (e) {

									}
								} else {
									return;
								}
							});
						} else {
							CallBackWgtUpdate();
						}
					}

				}, function(err) {

				});
			}, function(err) {

			});
		}
	}

	function examinationWgt() {
		function updateWgt(url) {
			mui.ajax(url, {
				data: param,
				dataType: 'json',
				type: 'post',
				timeout: 20000,
				processData:true,
				success: function(data) {
					// console.log(JSON.stringify(data));
					if (data && data["code"] ==0&& data["data"]  && data["data"]["update_url"] && data["data"]["update_url"].indexOf(".wgt") >= 0) {
						mui.confirm(data["data"]["update_content"], '发现新版本', ['立即更新', '下次再说'], function(e) {
							if (e.index == 0) {
								//data = data.replace(new RegExp("\"", 'g'), "");
								downWgtOrApk(data["data"]["update_url"], true);
							} else {
								return;
							}
						})
					}
				},
				error: function(xhr, type, errorThrown) {

				}
			});
		}

		plus.runtime.getProperty(plus.runtime.appid, function(inf) {
			var type;
			if (plus.os.name == 'iOS') {
				type = 'iOS';
			} else if (plus.os.name == 'Android') {
				type = 'Android';
			}
			updateWgt(wgtURL(type, inf.version));
		});
	}

	function downWgtOrApk(url, isWgt) {
		// console.log(url);
		var wgtWaiting = plus.nativeUI.showWaiting("下载文件...");
		var task = plus.downloader.createDownload(url, {
			filename: "_downloads/"
		}, function(d, status) {
			if (status == 200) {
				if (isWgt) {
					installWgt(d.filename); // 安装wgt包
				} else {
					installApk(d.filename); // 安装apk包
				}
			} else {
				mui.alert("下载失败！");
			}
			wgtWaiting.close();
		})

		task.addEventListener("statechanged", function(download, status) {
			switch (download.state) {
				case 2:
					wgtWaiting.setTitle("已连接到服务器");
					break;
				case 3:
					setTimeout(function() {
						if(download.totalSize<=0){
							var percent = download.downloadedSize / 1000;
							wgtWaiting.setTitle("已下载 " + parseInt(percent) + " kb");
						}else{
							var percent = download.downloadedSize / download.totalSize * 100;
							console.log(download.downloadedSize + "-" + download.totalSize);
							wgtWaiting.setTitle("已下载 " + parseInt(percent) + "%");
						}
						
					}, 0);
					break;
				case 4:
					wgtWaiting.setTitle("下载完成");
					break;
			}
		});
		task.start();
	};

	// 更新应用资源
	function installWgt(path) {
		plus.nativeUI.showWaiting("安装资源文件...");
		plus.runtime.install(path, {}, function() {
			plus.nativeUI.closeWaiting();
			plus.nativeUI.alert("应用资源更新完成！", function() {
				plus.runtime.restart();
			});
		}, function(e) {
			plus.nativeUI.closeWaiting();
			plus.nativeUI.alert("安装资源文件失败[" + e.code + "]：" + e.message);
		});
	}

	function installApk(path) {
		plus.runtime.install(path, {}, function() {

		}, function(e) {
			plus.nativeUI.closeWaiting();
			plus.nativeUI.alert("安装apk文件失败[" + e.code + "]：" + e.message);
		});
	}

	this.start = function() {
		mui.plusReady(function() {
			examinationInstallationPackage(function() {
				examinationWgt();
			});
		});
	}
}
