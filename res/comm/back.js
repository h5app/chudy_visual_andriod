function __stopPlay() {
	var allaudio = document.getElementsByTagName("audio");
	//console.log("__stopPlay allaudio=" + allaudio.length);
	for (var i = 0; i < allaudio.length; i++) {
		allaudio[i].pause();
		allaudio[i].currentTime = 0;
	}
	var allvideo = document.getElementsByTagName("video");
	//console.log("__stopPlay allvideo=" + allvideo.length);
	for (var i = 0; i < allvideo.length; i++) {
		allvideo[i].pause();
		allvideo[i].currentTime = 0;
	}
}

function __startPlay() {
	var allaudio = document.getElementsByTagName("audio");
	//console.log("__startPlay allaudio=" + allaudio.length);
	//有自动播放的属性，则设置开始播放
	for (var i = 0; i < allaudio.length; i++) {
		allaudio[i].currentTime = 0;
		if(allaudio[i].autoplay){
			allaudio[i].play();
		}
		//console.log("autoplay=" + allaudio[i].autoplay);
	}
	
	var allvideo = document.getElementsByTagName("video");
	//console.log("__startPlay allvideo=" + allvideo.length);
	for (var i = 0; i < allvideo.length; i++) {
		allvideo[i].currentTime = 0;
		if(allvideo[i].autoplay){
			allvideo[i].play();
		}
	}
}
