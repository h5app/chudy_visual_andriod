var request = require('request');
var ScreenReq = function() {};

var ____apihost = __config["comm"]["basedomain"];
console.log(____apihost)

ScreenReq.req(method, url, data, successcb,errorcb) {
	errorcb = errorcb || function(e){console.log(e)}
	var xhr = new plus.net.XMLHttpRequest();
	xhr.onreadystatechange = function() {
		console.log("onreadystatechange: " + xhr.readyState);
	}
	xhr.open(method || "POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	// 发送HTTP请求
	xhr.send(JSON.stringify(data));
	xhr.onload = function() {
		// xhr请求成功事件处理
		successcb(e);
	};
	xhr.onerror = function(e){
		errorcb(e);
	};
}
ScreenReq.loadContentPage = function(options, cb) {


	//1，从服务端读取针对当前设备发布的内容地址，并展示
	//2，从本地缓存中读取最后接收到的推送的内容地址，并展示
	//3，没有地址，则展示默认的首页，
	//http://127.0.0.1:8078/clientpage/opScreen.html
	ScreenReq.req(
		____apihost + '/client/getInfo', // 请求的URL
		method: 'GET', // 请求方法
		headers: { // 指定请求头
			'Accept-Language': 'zh-CN,zh;q=0.8'
			//,         // 指定 Accept-Language
			//'Cookie': '__utma=4454.11221.455353.21.143;' // 指定 Cookie
		}
	, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log(body) // 输出网页内容
			//默认为示例页面
			var loadPage = {
				"pushtype": 'screen',
				"data": {
					//opurl: ____apihost + "/clientpage/opScreen.html" //'http://127.0.0.1:8088/demo/opScreen.html'//"http://47.103.34.47:8088/demo/opScreen.html"
					//, viewurl: ____apihost + "/clientpage/viewScreen.html"
				}
			}
			cb(loadPage)
		} else {
			cb({})
		}
	})
}
/**
 * 客户端启动时，加载信息
 * 1，客户端注册信息
 * 2，客户端要展示的内容
 */
ScreenReq.loadClientInfo = function(formdata, cb) {
	//1，从服务端读取针对当前设备发布的内容地址，并展示
	//2，从本地缓存中读取最后接收到的推送的内容地址，并展示
	//3，没有地址，则展示默认的首页，
	//http://127.0.0.1:8078/clientpage/opScreen.html
	request({
		url: ____apihost + '/client/getClientInfo', // 请求的URL
		method: 'POST', // 请求方法
		headers: { // 指定请求头
		},
		form: formdata
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			console.log(body) // 输出网页内容
			if (typeof(body) == 'string') {
				body = JSON.parse(body);
			}
			cb(body)
		} else if (!error) {
			cb({
				"code": response.statusCode,
				"msg": response.statusMessage
			})
		} else if (error) {
			cb({
				"code": -10000,
				"msg": error
			})
		}

	})
}
/**
 * 客户端启动时，加载信息
 * 1，客户端注册信息
 * 2，客户端要展示的内容
 */
ScreenReq.regClientInfo = function(formdata, cb) {
	//1，从服务端读取针对当前设备发布的内容地址，并展示
	//2，从本地缓存中读取最后接收到的推送的内容地址，并展示
	//3，没有地址，则展示默认的首页，
	//http://127.0.0.1:8078/clientpage/opScreen.html
	request({
		url: ____apihost + '/client/regClientInfo', // 请求的URL
		method: 'POST', // 请求方法
		headers: { // 指定请求头
		},
		form: formdata
	}, function(error, response, body) {
		console.log(response) // 输出网页内容
		if (!error && response.statusCode == 200) {
			console.log(body) // 输出网页内容
			if (typeof(body) == 'string') {
				body = JSON.parse(body);
			}
			cb(body)
		} else if (!error) {
			cb({
				"code": response.statusCode,
				"msg": response.statusMessage
			})
		} else if (error) {
			cb({
				"code": -10000,
				"msg": error
			})
		}
	})
}
ScreenReq.loadLocationData = function(options, cb) {

	request({
		url: ____apihost + '/client/getLocationData/appid', // 请求的URL
		method: 'GET', // 请求方法
		headers: { // 指定请求头
			'Accept-Language': 'zh-CN,zh;q=0.8'

		}
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			if (typeof(body) == 'string') {
				body = JSON.parse(body);
			}
			cb(body)
		} else {
			cb(null)
		}
	})
}

ScreenReq.checkUpdate = function(options, cb) {

	request({
		url: ____apihost + '/client/getLocationData/appid', // 请求的URL
		method: 'GET', // 请求方法
		headers: { // 指定请求头
			'Accept-Language': 'zh-CN,zh;q=0.8'

		}
	}, function(error, response, body) {
		if (!error && response.statusCode == 200) {
			if (typeof(body) == 'string') {
				body = JSON.parse(body);
			}
			cb(body)
		} else {
			cb(null)
		}
	})
}
