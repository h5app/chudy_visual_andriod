# 触达云屏-Andriod大屏设备可视化大屏展示客户端

#### 介绍

用于在Andriod大屏设备上展示可视化大屏、企宣内容 的客户端软件

传送门：

[B站视频教程](https://space.bilibili.com/1642363989)

[触达云屏-官方文档](http://www.chudayun.com/docs/1.readme/)

![输入图片说明](doc/demo.jpg.png)

#### 功能介绍：

    基于UniApp 5+runtime开发
    支持Andriod6+
    支持web内容展示
    支持多屏自动轮播
    支持手动切换大屏
    支持设定每屏的播放时长
    支持主从设备互联、统一推送大屏
    支持展示触达云屏数据可视化大屏
    支持展示任意以网页形式展示的内容


#### 使用说明：

1.  下载发行版：下载地址 https://pan.baidu.com/s/1tO0-Yf3v3lj9W2tnAjci5A 提取码：cd9x

![输入图片说明](doc/image.png)

2.  安装客户端

3.  配置服务器信息
    暂不支持遥控器控制，需要连接键鼠操作。
    鼠标点击客户端页面右下角的状态内容
    填写推送服务主机信息（IP和端口）：
    端口填写 25002
    连接密码暂时未启用，不需要输入。
    点击【保存】，保存配置信息，保存后自动连接服务器
    连接成功则状态显示已连接。

4.  推送内容
    参考 Windows客户端介绍



#### 开发扩展


 
#### 参与贡献

1.  Fork 本仓库
2.  新建 Feat_xxx 分支
3.  提交代码
4.  新建 Pull Request


#### 需求、建议

    欢迎大家反馈需求和建议。 沟通探讨QQ群：160269112
