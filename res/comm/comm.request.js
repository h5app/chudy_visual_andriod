var ScreenReq = function() {};

var ____apihost = _cfg.getRCfg()["basedomain"];

ScreenReq.req = function(method, url, data, successcb, errorcb) {
	errorcb = errorcb || function(e) {
		console.log(e)
	}
	var xhr = new plus.net.XMLHttpRequest();
	xhr.onreadystatechange = function() {
		console.log("onreadystatechange: " + xhr.readyState);
		switch (xhr.readyState) {
			case 0:
				// console.log( "xhr请求已初始化" );
				break;
			case 1:
				// console.log( "xhr请求已打开" );
				break;
			case 2:
				// console.log( "xhr请求已发送" );
				break;
			case 3:
				// console.log( "xhr请求已响应");
				break;
			case 4:
				// console.log(xhr.status);
				var body = null;
				if (xhr.status == 200) {
					console.log("xhr请求成功：");
					if (typeof(xhr.responseText) == 'string') {
						body = JSON.parse(xhr.responseText);
					}
					successcb(body)
				} else {
					console.log("xhr请求失败：" + xhr.readyState);
					successcb(body)
				}
				break;
			default:
				break;
		}
	}
	xhr.open(method || "POST", url);
	xhr.setRequestHeader('Content-Type', 'application/json');
	// 发送HTTP请求
	xhr.send(JSON.stringify(data));
	xhr.onload = function(e) {
		// xhr请求成功事件处理
	};
	xhr.onerror = function(e) {
		errorcb(e);
	};

}

ScreenReq.request = function(options, successcb, errorcb) {
	if (!options) {
		options = {};
	}
	//method, url, data, successcb,errorcb
	var errorcb = errorcb || function(e) {
		console.log(e)
	}
	var xhr = new plus.net.XMLHttpRequest();
	xhr.onreadystatechange = function() {
		console.log("onreadystatechange: " + xhr.readyState);
		switch (xhr.readyState) {
			case 0:
				// console.log( "xhr请求已初始化" );
				break;
			case 1:
				// console.log( "xhr请求已打开" );
				break;
			case 2:
				// console.log( "xhr请求已发送" );
				break;
			case 3:
				// console.log( "xhr请求已响应");
				break;
			case 4:
				// console.log(xhr.status);
				var body = null;
				if (xhr.status == 200) {
					console.log("xhr请求成功：");
					if (typeof(xhr.responseText) == 'string') {
						body = JSON.parse(xhr.responseText);
					}
					successcb(body)
				} else {
					console.log("xhr请求失败：" + xhr.readyState);
					successcb(body)
				}
				break;
			default:
				break;
		}
	}
	xhr.open(options["method"] || "POST", options["url"] || "");

	var defaultHeaders = {
		'Content-Type': 'application/json'
	}
	var headers = options["headers"] || defaultHeaders;
	for (var h in headers) {
		xhr.setRequestHeader(h, headers[h]);
	}

	var data = options["data"] || {};
	// 发送HTTP请求
	xhr.send(JSON.stringify(data));
	xhr.onload = function(e) {
		// xhr请求成功事件处理
	};
	xhr.onerror = function(e) {
		errorcb(e);
	};

}
ScreenReq.loadContentPage = function(options, cb) {


	//1，从服务端读取针对当前设备发布的内容地址，并展示
	//2，从本地缓存中读取最后接收到的推送的内容地址，并展示
	//3，没有地址，则展示默认的首页，
	//http://127.0.0.1:8078/clientpage/opScreen.html
	// var loadPage = {
	// 	"pushtype": 'screen',
	// 	"data": {
	// 		//opurl: ____apihost + "/clientpage/opScreen.html" //'http://127.0.0.1:8088/demo/opScreen.html'//"http://47.103.34.47:8088/demo/opScreen.html"
	// 		//, viewurl: ____apihost + "/clientpage/viewScreen.html"
	// 	}
	// }
}
/**
 * 客户端启动时，加载信息
 * 1，客户端注册信息
 * 2，客户端要展示的内容
 */
ScreenReq.loadClientInfo = function(formdata, cb) {
	//1，从服务端读取针对当前设备发布的内容地址，并展示
	//2，从本地缓存中读取最后接收到的推送的内容地址，并展示
	//3，没有地址，则展示默认的首页，
	//http://127.0.0.1:8078/clientpage/opScreen.html
	// console.log(JSON.stringify(formdata));
	// mui.ajax(____apihost + '/client/getClientInfo', {
	// 	data: formdata,
	// 	processData:true,
	// 	dataType: 'json', //服务器返回json格式数据
	// 	type: 'post', //HTTP请求类型
	// 	timeout: 10000, //超时时间设置为10秒；
	// 	success: function(data) {
	// 		//服务器返回响应，根据响应结果，分析是否登录成功；
	// 		cb(data);
	// 	},
	// 	error: function(xhr, type, errorThrown) {
	// 		//异常处理；
	// 		console.log(type);
	// 	}
	// });
	cb({"code":0});
}


//初始化注册时部门选择
ScreenReq.initDepartSelect = function(formdata,cb){
	/**
	 * 初始化注册时部门选择
	 * **/
	mui.ajax({
		url: ____apihost + '/client/initDepartSelectData',   // 请求的URL
		method: 'post',                   // 请求方法
		headers: {v:6666},
		data:formdata,
		dataType: 'json', //服务器返回json格式数据
		timeout: 10000,//超时时间设置为10秒；
		success: function(data) {
			
			cb(data);
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
			cb({
				"code":-10000,
				"msg":error
			})
		}
	})
}

ScreenReq.clientLogin = function(formdata,cb){

	/**
	 * 验证租户代码密码
	 * **/
	mui.ajax({
		url: ____apihost + '/client/clientLogin',   // 请求的URL
		method: 'post',                   // 请求方法
		headers: {                       // 指定请求头			
		},
		data:formdata,
		dataType: 'json', //服务器返回json格式数据
		timeout: 10000,//超时时间设置为10秒；
		success: function(data) {
			
			cb(data);
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
			cb({
				"code":-10000,
				"msg":error
			})
		}
	})

}

ScreenReq.updateClientInfo = function(formdata,cb){

	/**
	 * 修改绑定部门信息
	 * **/
	mui.ajax({
		url: ____apihost + '/client/updateClientInfo',   // 请求的URL
		method: 'post',                   // 请求方法
		headers: {                       // 指定请求头
		},
		data:formdata,
		dataType: 'json', //服务器返回json格式数据
		timeout: 10000,//超时时间设置为10秒；
		success: function(data) {
			
			cb(data);
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
			cb({
				"code":-10000,
				"msg":error
			})
		}
	})
}





/**
 * 客户端启动时，加载信息
 * 1，客户端注册信息
 * 2，客户端要展示的内容
 */
ScreenReq.regClientInfo = function(formdata, cb) {
	//1，从服务端读取针对当前设备发布的内容地址，并展示
	//2，从本地缓存中读取最后接收到的推送的内容地址，并展示
	//3，没有地址，则展示默认的首页，
	//http://127.0.0.1:8078/clientpage/opScreen.html
	mui.ajax(____apihost + '/client/regClientInfo', {
		data: formdata,
		dataType: 'json', //服务器返回json格式数据
		processData:true,
		type: 'post', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
		},
		success: function(data) {
			//服务器返回响应，根据响应结果，分析是否登录成功；
			cb(data);
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
		}
	});
}
ScreenReq.loadLocationData = function(options, cb) {
	
	mui.ajax(____apihost + '/client/getLocationData/appid', {
		data: {},
		dataType: 'json', //服务器返回json格式数据
		type: 'get', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: function(data) {
			//服务器返回响应，根据响应结果，分析是否登录成功；
			cb(data);
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
		}
	});
}

ScreenReq.checkUpdate = function() {
	var param = {
		"apihost": ____apihost,
		"pvcode": 0,
		"vcode": plus.runtime.versionCode,
		"sysinfo": JSON.stringify(____clientStaticData),
		"appid": VisualConst.getCustomDevice()["appid"]
	};
	console.log("checkUpdate---------" + JSON.stringify(param));
	new ApplicationUpdateTool(param).start();
}

ScreenReq.eventCallback = function (option,data, cb) {
	var postdata = {
			"id":VisualConst.getCustomDevice()["deviceid"],
			"eventkey": option['data']['eventkey'],
			"data":data
		};
	console.log(JSON.stringify(postdata));
	mui.ajax(option['data']['apiurl'], {
		data: postdata,
		dataType: 'json', //服务器返回json格式数据
		processData:true,
		type: 'post', //HTTP请求类型
		timeout: 10000, //超时时间设置为10秒；
		headers: {
			'Content-Type': 'application/json'
		},
		success: function(data) {
			//服务器返回响应，根据响应结果，分析是否登录成功；
			cb(data);
		},
		error: function(xhr, type, errorThrown) {
			//异常处理；
			console.log(type);
			cb(null);
		}
	});
}