var _mainWv=null;
// H5 plus事件处理
function plusReady(){
	_mainWv=plus.webview.getLaunchWebview();
}
if(window.plus){
	plusReady();
}else{
	document.addEventListener('plusready', plusReady, false);
}
______sendTouch=function(t){
	if(_mainWv){
		_mainWv.evalJS('touchContentPage(' + JSON.stringify(t) + ');'); //在指定webview 执行js脚本
	}
}
//以下代码 和PC端代码保持一致
var ______touchRegion = 1;//0.2;//滑动起点在屏幕左右两侧的范围比例
var ______clickRegion = 0.2;//滑动起点在屏幕左右两侧的范围比例
var ______touchMin = 10;//像素
var ______touchDistance = 0.2;//滑动距离 占  屏幕的宽度比例

______upOrDown = function(startX, startY, endX, endY) {
	const that = this;
	let direction = that.GetSlideDirection(startX, startY, endX, endY);
	var direction_name = "";
	switch (direction) {
		case 0:
			console.log("没滑动");
			direction_name = "没滑动";
			break;
		case 1:
			console.log("向上");
			direction_name = "向上";
			break;
		case 2:
			console.log("向下");
			direction_name = "向下";
			break;
		case 3:
			console.log("向左");
			direction_name = "向左";
			break;
		case 4:
			console.log("向右");
			direction_name = "向右";
			break;
		default:
			direction_name = "未知";
			break;
	}
	return {
		"direction": direction,
		"remark": direction_name
	}
};
//根据起点和终点返回方向 1：向上，2：向下，3：向左，4：向右,0：未滑动
//活动距离太短，则认为没有滑动，滑动的起点不在指定范围内，则认为不是滑动切换屏幕
GetSlideDirection = function(startX, startY, endX, endY) {
	const that = this;
	let dy = startY - endY;
	let dx = endX - startX;
	let result = 0;
	//如果滑动距离太短
	var xDrift = Math.abs(dx);
	var yDrift = Math.abs(dy);
	if (xDrift < ______touchMin && yDrift < ______touchMin) {
		return result;
	}
	const screenWidth = window.innerWidth;
	const tr = (screenWidth * ______touchRegion);//在不在约定的滑动起始区域
	const td = (screenWidth * ______touchDistance);//满足不满足约定的滑动距离
	console.log("screenWidth=" + screenWidth + ";tr=" + tr + ";td=" + td)
	console.log("xDrift=" + xDrift + ";startX=" + startX)
	if(xDrift < td){
		//左右滑动距离超过屏幕宽度的指定比例时
		return result;
	}
	if(tr < startX && startX <(screenWidth-tr)){
		//滑动的起点没在屏幕两侧的指定范围内
		return result;
	}
	
	let angle = that.GetSlideAngle(dx, dy);
	if (angle >= -45 && angle < 45) {
		result = 4;
	} else if (angle >= 45 && angle < 135) {
		result = 1;
	} else if (angle >= -135 && angle < -45) {
		result = 2;
	} else if ((angle >= 135 && angle <= 180) || (angle >= -180 && angle < -135)) {
		result = 3;
	}
	return result;
};
//返回角度
GetSlideAngle = function(dx, dy) {
	return Math.atan2(dy, dx) * 180 / Math.PI;
}
var startX, startY, endX, endY;

function ______load() {
	console.log(
		'______load ----------------------------------------------------------------------------------------------------------------------'
	)
	//触摸事件绑定
	function ______touch(event) {
		var event = event || window.event;
		/*
		var touch = event.targetTouches[0];
		*/
		switch (event.type) {
			case "touchstart":
				startX = event.touches[0].pageX;
				startY = event.touches[0].pageY;
				event = event || window.event;
				break;
			case "touchend":
				endX = event.changedTouches[0].pageX;
				endY = event.changedTouches[0].pageY;
				var res = ______upOrDown(startX, startY, endX, endY);
				res["_type"] = "touchend";
				// {
				// 	"direction": 3,
				// 	"remark": "向左"
				// }
				______sendTouch(res);
				break;
			case "touchmove":
				break;
		}
	}

	function ______mouse(event) {
		var event = event || window.event;

		switch (event.type) {
			case "mousedown":
				break;

			case "mouseup":
				break;

			case "mousemove":
				//event.preventDefault();		 
				break;
			case "click":
				console.log(event);
				const leftInstance = event.screenX;
				const rightInstance = event.screenY;
				const screenWidth = window.innerWidth;
				const bj = (screenWidth * ______clickRegion);
				if (leftInstance < bj) {
					//3 向左
					console.log('靠左边');
					______sendTouch({
						"direction": 3,
						"remark": "向左"
					});
				} else if (leftInstance > (screenWidth - bj)) {
					//4 向右
					console.log('靠右边');
					______sendTouch({
						"direction": 4,
						"remark": "向右"
					})
				} else {
					console.log('中间');
					______sendTouch({
						"direction": 0,
						"remark": "没有滑动"
					})
				}
				break;
		}
	}
	var hasTouch = function() {
		var touchObj = {};
		touchObj.isSupportTouch = "ontouchend" in document ? true : false;
		touchObj.isEvent = touchObj.isSupportTouch ? 'touchstart' : 'click';
		return touchObj.isEvent;
	};
	if (hasTouch() == "touchstart") {
		document.addEventListener('touchstart', ______touch, false);
		document.addEventListener('touchmove', ______touch, false);
		document.addEventListener('touchend', ______touch, false);
		console.log("浏览器支持触屏");
	} else {
		console.log("浏览器不支持触屏");
		document.addEventListener('click', ______mouse, false);
		document.addEventListener('mousedown', ______mouse, false);
		//document.addEventListener('mousemove', ______mouse, false);
		document.addEventListener('mouseup', ______mouse, false);
	}

}
______load();
//window.addEventListener('load', ______load, false);