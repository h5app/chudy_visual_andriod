/*得到手机MAC地址*/

			function getNetWork() {

				var mac = "xxx-xxx-xxx-xxx";

				if (plus.os.name == "Android") {

					//WifiManager

					var Context = plus.android.importClass("android.content.Context");

					var WifiManager = plus.android.importClass("android.net.wifi.WifiManager");

					var wifiManager = plus.android.runtimeMainActivity().getSystemService(Context.WIFI_SERVICE);

					var WifiInfo = plus.android.importClass("android.net.wifi.WifiInfo");

					var wifiInfo = wifiManager.getConnectionInfo();

					mac = wifiInfo.getMacAddress();

				}

				return mac;

			}

			/*获取手机内存信息*/
			function getMemorySize() {
				var memoryInfo = '';
				if (plus.os.name == "Android") {
					var Context = plus.android.importClass("android.content.Context");
					var ActivityManager = plus.android.importClass("android.app.ActivityManager");
					var mi = new ActivityManager.MemoryInfo();
					var activityService = plus.android.runtimeMainActivity().getSystemService(Context.ACTIVITY_SERVICE);
					activityService.getMemoryInfo(mi);
					memoryInfo = mi.plusGetAttribute("availMem");
				}
				return memoryInfo;
			}
			/*获取手机内部总的存储空间*/

			function getTotalInternalMemorySize() {

				var internalMemSize = 0;

				if (plus.os.name == "Android") {

					var environment = plus.android.importClass("android.os.Environment");

					var statFs = plus.android.importClass("android.os.StatFs");

					var files = plus.android.importClass("java.io.File");



					var Files = environment.getDataDirectory();

					var StatFs = new statFs(Files.getPath());

					var blockSize = parseFloat(StatFs.getBlockSize());

					var blockCount = parseFloat(StatFs.getBlockCount());

					internalMemSize = blockSize * blockCount;

				}

				return internalMemSize;

			}

			/*获取总内存*/

			function getTotalRamSize() {

				var memInfo = '/proc/meminfo';

				var temp = '',

					ramSize = '',

					arrays, initMemory;

				var fileReader = plus.android.importClass("java.io.FileReader");

				var bufferedReader = plus.android.importClass("java.io.BufferedReader");

				var FileReader = new fileReader(memInfo);

				var BufferedReader = new bufferedReader(FileReader, 8192);

				while ((temp = BufferedReader.readLine()) != null) {

					if (-1 != temp.indexOf('MemTotal:')) {

						var value = temp.replace(/[^0-9]/ig, "");

						ramSize = Math.floor(parseInt(value) / (1024));

					}

				}



				return ramSize;

			}

			/*获取手机CPU信息*/

			function getCpuInfo() {
				var cpuInfo = '/proc/cpuinfo';
				var temp = '',
					cpuHardware;
				var fileReader = plus.android.importClass("java.io.FileReader");
				var bufferedReader = plus.android.importClass("java.io.BufferedReader");
				var FileReader = new fileReader(cpuInfo);
				var BufferedReader = new bufferedReader(FileReader, 8192);
				while ((temp = BufferedReader.readLine()) != null) {
					if (-1 != temp.indexOf('Hardware')) {
						cpuHardware = temp.substr(parseInt(temp.indexOf(":")) + 1);
					}
				}
				return cpuHardware;
			}
			/*获取CPU核数*/
			function getCpuCount() {

				var Runtime = plus.android.importClass("java.lang.Runtime");

				var cpuCount = Runtime.getRuntime().availableProcessors();

				return cpuCount;

			}

			function getMemInfo() {
				plus.android.importClass('java.io.BufferedReader')
				var localFileReader = plus.android.newObject('java.io.FileReader', '/proc/meminfo')
				var localBufferedReader = plus.android.newObject('java.io.BufferedReader', localFileReader, 8192)
				var str = localBufferedReader.readLine().toString();
				var totalMemStr = str;
				var avaMemStr = ''
				var i = 0
				while (i < 2) {
					str = localBufferedReader.readLine().toString();
					avaMemStr = str;
					i++
				}
				totalMemStr = (parseInt(totalMemStr.toUpperCase().replace(/(( )|(:)|[A-Z])/gi, '')) / 1024).toFixed(0)
				avaMemStr = (parseInt(avaMemStr.toUpperCase().replace(/(( )|(:)|[A-Z])/gi, '')) / 1024).toFixed(0)
				console.log(totalMemStr);
				console.log(avaMemStr);
				return {
					total: totalMemStr,
					ava: avaMemStr
				}
			}
			// 创建并显示新窗口
			function createNewWindow(url) {
				console.log("创建新窗口");
				var w = plus.webview.create(url);
				w.show(); // 显示窗口
			}