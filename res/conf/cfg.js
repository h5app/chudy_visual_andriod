var __config = {
	"comm":{
		"appid":"screen_00000002",
		"runmode":"dev"
	},
	"about":{
		"website":"http://www.chudayun.comm",
		"tel":"12345678"
	},
	"runcfg":{
		"dev":{
			"basedomain":"http://192.168.3.14:8078",
			"ws_ip":"192.168.3.14",
			"ws_port":"25002",
			"ws_protocol":"ws",
			"update_url":"/update",
			"location_url":"/update",
			"deviceinfo_url":"/getClientinfo"
		},"out":{
			"basedomain":"http://datavisual.hezyun.cn:8078",
			"ws_ip":"datavisual.hezyun.cn",
			"ws_port":"9326",
			"ws_protocol":"ws",
			"update_url":"/update",
			"location_url":"/update",
			"deviceinfo_url":"/getClientinfo"
		}
	}
};
var _cfg = function () { };
_cfg.getRCfg = function(){
	return __config["runcfg"][__config["comm"]["runmode"]];
}