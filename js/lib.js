// JQuery方法定制
(function($) {

   //设置全局beforesend
   mui.ajaxSettings.beforeSend = function(xhr, setting) {
	var _token = Lib.getStorage('_token');
	var _deviceId = Lib.getStorage('_deviceId');
	//Lib.delStorage('_token');
	xhr.setRequestHeader("sessionkey",_token); 
	xhr.setRequestHeader("imei",_deviceId); 

	console.log('beforeSend:::' + JSON.stringify(setting));
   };
   
   //设置全局complete
   mui.ajaxSettings.complete = function(xhr, status) {
	console.log('complete:::' + status);
   }


	

    //表单实例化
    $.fn.serializeJson = function () {
        var serializeObj = {};
        var array = this.serializeArray();
        $(array).each(
            function () {
                if (serializeObj[this.name]) {
                    if ($.isArray(serializeObj[this.name])) {
                        serializeObj[this.name].push(this.value);
                    } else {
                        serializeObj[this.name] = [
                            serializeObj[this.name], this.value];
                    }
                } else {
                    serializeObj[this.name] = this.value;
                }
            });
        return serializeObj;
    };


}(jQuery))


var Lib = function () { }
Lib.getStorage = function (key) {
	return plus.storage.getItem(key);
}
Lib.setStorage = function (key, data, cb) {
	plus.storage.setItem(key,data);
    return true;
}
Lib.delStorage = function(key){
    plus.storage.removeItem(key);
}

Lib.getRequestHeaders = function(){
    //Lib.delStorage('_token');
    var _token = Lib.getStorage('_token');
    var _deviceId = Lib.getStorage('_deviceId');
    //var _tenantId = Lib.getStorage('_tenantId');
    return {"sessionkey": _token,"imei":_deviceId};
}

